# 1.Mongodb
start service mongodb

# 2.Server
cd server
edit .env
yarn
cp .env.example .env
yarn dev
localhost:5000 (env))

# 3.Client
cd client
edit .env
npm install
npm start
localhost:5001 (env))
