const mongoose = require('mongoose');
const httpStatus = require('http-status');
const { omitBy, isNil } = require('lodash');
const bcrypt = require('bcryptjs');
const moment = require('moment-timezone');
const jwt = require('jwt-simple');
const uuidv4 = require('uuid/v4');
const APIError = require('../utils/APIError');
const { env, jwtSecret, jwtExpirationInterval } = require('../../config/vars');

/**
* User Types
*/
const types = ['user', 'worker'];

/**
 * User Schema
 * @private
 */
const workerSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId
  },
  range_price: {
    min: String,
    max: String
  },
  job_id: {
    type: mongoose.Schema.Types.ObjectId,
    require: true
  },
  state: {
    type: mongoose.Schema.Types.ObjectId,
    require: true
  },
  city: {
    type: mongoose.Schema.Types.ObjectId,
    require: true
  },
  country: {
    type: mongoose.Schema.Types.ObjectId,
    require: true
  },
  full_address: {
    type: String,
    maxlength: 999
  },
  location: {
    lat: String,
    long: String
  },
  email: {
    type: String,
    match: /^\S+@\S+\.\S+$/,
    required: true,
    unique: true,
    trim: true,
    lowercase: true,
  },
  is_premium: {
    type: Boolean,
    default: false
  },
  is_vip: {
    type: Boolean,
    default: false
  },
  status: {
    type: String,
    maxlength: 10,
    default: "inactive"
  }
}, {
    timestamps: true,
  });

  /**
 * Methods
 */
workerSchema.method({
    transform() {
      const transformed = {};
      const fields = ['id', 'name', 'email', 'picture', 'role', 'createdAt'];
  
      fields.forEach((field) => {
        transformed[field] = this[field];
      });
  
      return transformed;
    }
  });
  
  /**
   * Statics
   */
  workerSchema.statics = {
  
    types,
  
    /**
     * Get user
     *
     * @param {ObjectId} id - The objectId of user.
     * @returns {Promise<User, APIError>}
     */
    async get(id) {
      try {
        let user;
  
        if (mongoose.Types.ObjectId.isValid(id)) {
          user = await this.findById(id).exec();
        }
        if (user) {
          return user;
        }
  
        throw new APIError({
          message: 'User does not exist',
          status: httpStatus.NOT_FOUND,
        });
      } catch (error) {
        throw error;
      }
    },
  
    /**
     * List users in descending order of 'createdAt' timestamp.
     *
     * @param {number} skip - Number of users to be skipped.
     * @param {number} limit - Limit number of users to be returned.
     * @returns {Promise<User[]>}
     */
    list({
      page = 1, perPage = 30, firstname, lastname, email, type,
    }) {
      const options = omitBy({ firstname, lastname, email, type }, isNil);
  
      return this.find(options)
        .sort({ createdAt: -1 })
        .skip(perPage * (page - 1))
        .limit(perPage)
        .exec();
    }
  };
  
  /**
   * @typedef Worker
   */
  module.exports = mongoose.model('Worker', workerSchema);
  