/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import ReactTable from "react-table";
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import {
  makeSelectRepos,
  makeSelectLoading,
  makeSelectError,
} from 'containers/App/selectors';
import H2 from 'components/H2';
import ReposList from 'components/ReposList';
import AtPrefix from './AtPrefix';
import CenteredSection from './CenteredSection';
import Form from './Form';
import Input from './Input';
import Section from './Section';
import messages from './messages';
import { loadRepos } from '../App/actions';
import { changeUsername } from './actions';
import { makeSelectUsername } from './selectors';
import reducer from './reducer';
import saga from './saga';

import 'react-table/react-table.css'

const data = [
  {
    sym: 'TRX/BTC',
    hour: 1,
    pattern: 'Hanging man',
    signal: 'bearish',
    last_price: '0.00123',
    predict_process: '85',
    last_time: '09:52'
  },
  {
    sym: 'TRX/BTC',
    hour: 1,
    pattern: 'Hanging man',
    signal: 'bearish',
    last_price: '0.00123',
    predict_process: '85',
    last_time: '09:52'
  },
  {
    sym: 'TRX/BTC',
    hour: 1,
    pattern: 'Hanging man',
    signal: 'bullish',
    last_price: '0.00123',
    predict_process: '85',
    last_time: '09:52'
  },
  {
    sym: 'TRX/BTC',
    hour: 1,
    pattern: 'Hanging man',
    signal: 'bullish',
    last_price: '0.00123',
    predict_process: '85',
    last_time: '09:52'
  },
];

/* eslint-disable react/prefer-stateless-function */
export class HomePage extends React.PureComponent {
  /**
   * when initial state username is not null, submit the form to load repos
   */
  componentDidMount() {
    if (this.props.username && this.props.username.trim().length > 0) {
      this.props.onSubmitForm();
    }
  }

  render() {
    const { loading, error, repos } = this.props;
    const reposListProps = {
      loading,
      error,
      repos,
    };

    return (
      <article>
        <Helmet>
          <title>Home Page</title>
          <meta
            name="description"
            content="A React.js Boilerplate application homepage"
          />
        </Helmet>
        <div>
          <CenteredSection>
            <H2>
              <FormattedMessage {...messages.startProjectHeader} />
            </H2>
            <p>
              <FormattedMessage {...messages.startProjectMessage} />
            </p>
          </CenteredSection>

          <CenteredSection>
            <ReactTable
              data={data}
              columns={[
                {
                  Header: 'Crypto Currency',
                  columns: [{
                    Header: 'Symbol',
                    accessor: 'sym'
                  }, {
                    Header: 'Last Price',
                    accessor: 'last_price'
                  }]
                },
                {
                  Header: 'Prediction',
                  columns: [
                    {
                      Header: "Pattern",
                      accessor: 'pattern'
                    },
                    {
                      Header: 'Predict trend',
                      accessor: 'predict_process',
                      Cell: row => (
                        <div
                          style={{
                            width: '100%',
                            height: '100%',
                            backgroundColor: '#dadada',
                            borderRadius: '2px'
                          }}
                        >
                          <div
                            style={{
                              width: `${row.value}%`,
                              height: '100%',
                              backgroundColor: row.value > 66 ? '#85cc00'
                                : row.value > 33 ? '#ffbf00'
                                  : '#ff2e00',
                              borderRadius: '2px',
                              transition: 'all .2s ease-out'
                            }}
                          />
                        </div>
                      )
                    }, {
                      Header: 'Trend',
                      accessor: 'signal',
                      Cell: row => (
                        <span>
                          <span style={{
                            color: row.value === 'relationship' ? '#ff2e00'
                              : row.value === 'bearish' ? 'red'
                                : 'green',
                            transition: 'all .3s ease'
                          }}>
                            &#x25cf;
                    </span> {
                            row.value === 'relationship' ? 'In a relationship'
                              : row.value === 'bearish' ? `bearish`
                                : 'bullish'
                          }
                        </span>
                      )
                    }]
                }
              ]}
              defaultPageSize={10}
              className="-striped -highlight"
            />
          </CenteredSection>

          <Section>
            <H2>
              <FormattedMessage {...messages.trymeHeader} />
            </H2>
            <Form onSubmit={this.props.onSubmitForm}>
              <label htmlFor="username">
                <FormattedMessage {...messages.trymeMessage} />
                <AtPrefix>
                  <FormattedMessage {...messages.trymeAtPrefix} />
                </AtPrefix>
                <Input
                  id="username"
                  type="text"
                  placeholder="mxstbr"
                  value={this.props.username}
                  onChange={this.props.onChangeUsername}
                />
              </label>
            </Form>
            <ReposList {...reposListProps} />
          </Section>
        </div>
      </article>
    );
  }
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  onSubmitForm: PropTypes.func,
  username: PropTypes.string,
  onChangeUsername: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: evt => dispatch(changeUsername(evt.target.value)),
    onSubmitForm: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadRepos());
    },
  };
}

const mapStateToProps = createStructuredSelector({
  repos: makeSelectRepos(),
  username: makeSelectUsername(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
